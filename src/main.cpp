#include <string>
#include <fstream>
#include <iostream>
#include "easycolor.hpp"

int main() 
{   
    bool op = true;
    std::string ProjectName;
    std::string Compiler;
    std::string Output;
    std::string Source;
    std::string Include;
    std::string CompilerOptions; 

    std::cout << ANSI_COLOR_BRIGHT_RED << "EasyMakeBuilder" << "\n" << ANSI_COLOR_RESET << ANSI_BLINK << "//Made By CodePoison;"<< ANSI_COLOR_RESET << "\n\n" << std::endl;    
    
    std::cout << ANSI_COLOR_BLUE << "Name of Project: ";
    std::cin >> ProjectName; 
    
    std::cout << "Compiler: ";
    std::cin >> Compiler; 
    
    std::cout << "Output File Name: ";    
    std::cin >> Output; 
    
    std::cout << "Sources (example: src/*.c): ";    
    std::cin >> Source;
    
    std::cout << "Include Folder: ";    
    std::cin >> Include;
    
    std::cout << "Extra Compiler Options (example: \"option\", \"another option\" or just press enter to skip): ";    
    while(op == true) 
    {   
        std::cin.getline();
        if (std::cin.getline() == '\n')
        {
            op = false;
        }

    }

    //hard coded until easyjson comes out
    std::ofstream file("autobuild.ezmk");
    file << "{" << std::endl;
    file << "  \"project\":\"" << ProjectName << "\"," << std::endl;
    file << "  \"compiler\":\"" << Compiler << "\"," << std::endl;
    file << "  \"output\":\"" << Output << "\"," << std::endl;
    file << std::endl;
    file << "  \"includes\": [" << std::endl;
    file << "      \"" << Include << "\"" << std::endl; 
    file << "  ],";
    file << std::endl;
    file << "  \"sources\": [" << std::endl;
    file << "    \"" << Source << "\"" << std::endl; 
    file << "  ],";
    file << std::endl;
    file << "  \"compiler_options\": [" << std::endl;
    file << "    \"" << CompilerOptions << "\"" << std::endl; 
    file << "  ]," << std::endl;
    file << "}" << std::endl;

    std::cout << "Build file has been made" << std::endl;
}
